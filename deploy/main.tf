terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-1234567"
    key            = "recipe-app.tfstate"
    region         = "us-west-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-loc2"
  }
}

provider "aws" {
  region = "us-west-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}