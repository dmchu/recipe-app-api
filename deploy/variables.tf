variable "prefix" {
  default = "recipe"
}

variable "project" {
  default = "recipe-app-api"
}

variable "contact" {
  default = "dmitry.chuveev@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

#variable "ecr_image_api" {
#  description = "ECR Image for API"
#  default     = "417269602512.dkr.ecr.us-east-1.amazonaws.com/server-architecture-and-its-usecase-devops:latest"
#}
#
#variable "ecr_image_proxy" {
#  description = "ECR Image for API"
#  default     = "417269602512.dkr.ecr.us-east-1.amazonaws.com/server-architecture-and-its-usecase:latest"
#}
#
#variable "django_secret_key" {
#  description = "Secret key for Django app"
#}
